public class Room {
    private int roomNr;
    private int roomCapacity;
    private boolean isBathroom;
    private boolean isAvailable;
// deklaruje zmienne

    public Room(int roomNr, int roomCapacity, boolean isBathroom, boolean isAvailable) {
        this.roomNr = roomNr;
        this.roomCapacity = roomCapacity;
        this.isBathroom = isBathroom;
        this.isAvailable = isAvailable;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomNr=" + roomNr +
                ", roomCapacity=" + roomCapacity +
                ", isBathroom=" + isBathroom +
                ", isAvailable=" + isAvailable +
                '}';
    }//tworzę metodę która wyświetla pojedynczy pokój z parametrami

    public boolean isAvailable() {
        return isAvailable;
    }

    public int getRoomNumber() {
        return roomNr;
    }

    public void setRoomNr(int roomNr) {
        this.roomNr = roomNr;
    }

    public void setRoomCapacity(int roomCapacity) {
        this.roomCapacity = roomCapacity;
    }

    public void setBathroom(boolean bathroom) {
        isBathroom = bathroom;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }
}