import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class HotelService {
    private Hotel hotel;
    private Logger LOG;

    private static final String BOOKING_IMPOSIBLE = "Room cannot be booked: ";
    private static final String ROOM_BOOKED_SUCESSFULLY = "Room booked sucessfully : ";
    private static final String ROOM_FREED = "Room sucesfully freed : ";
    private static final String ROOM_ALREADY_AVALIABLE = "Room already avaliable";

    public HotelService(Hotel hotel) {
        LOG = Logger.getLogger(HotelService.class.getName());
        this.hotel = hotel;
    }
    //tworze konstruktor dla Hotelu

    List<Room> getListOfRooms() {
        return hotel.getListOfRooms();
    }

    public void showRooms() {
        for (int i = 0; i < getListOfRooms().size(); i++) {
            System.out.println(getListOfRooms().get(i));
        }//tworzę metodę wyswietlającą liste pokoi z parametrami
    }

    public List<Room> getLisOfAvaliableRooms() {
        List<Room> helpList = new ArrayList<>();
        for (Room room : getListOfRooms()) {
            if (room.isAvailable()) {
                helpList.add(room);
            }
        }
        return helpList;
    }

    public void showAvailable() {
        for (int i = 0; i < getLisOfAvaliableRooms().size(); i++) {
            System.out.println(getLisOfAvaliableRooms().get(i));
        }
    }

    public boolean bookRoom(int roomNo) {
        boolean isBooked = false;
        for (Room room : getListOfRooms()) {
            if (room.getRoomNumber() == roomNo) {
                if (room.isAvailable()) {
                    room.setAvailable(false);
                    System.out.println("Booking successfull for room number:" + room.getRoomNumber());
                    isBooked = true;

                } else {
                    System.out.println("Booking not available");
                }
            }
        }
        return isBooked;
    }

    public boolean freeRoom(int roomNo) {
        boolean isFree = false;
        for (Room room : getListOfRooms()) {
            if (room.getRoomNumber() == roomNo) {
                if (room.isAvailable()) {
                    room.setAvailable(true);
                    System.out.println("Room number:" + room.getRoomNumber() + " is no longer booked ");
                    isFree = true;

                } else {
                    System.out.println("This room is already free");
                }
            }
        }return isFree;
    }
}

//room.setAvailable(false);
//                System.out.println("Room" room + "booked sucessfully");
